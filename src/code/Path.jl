module Path

using LinearAlgebra
using StaticArrays

export MomentumSpacePath, d₀

const d₀ = 1.0
const OUT_OF_PLANE_MOMENTUM = π / d₀ * SA[0, 0, sqrt(3)]

struct MomentumSpacePath
    points::Int

    X₀::SVector{3,Float64}
    Γ₀::SVector{3,Float64}
    Y₀::SVector{3,Float64}
    Γ!₀::SVector{3,Float64}
    M₀::SVector{3,Float64}
end

function MomentumSpacePath(points)
    X₀ = 2π / 3d₀ * SA[0, sqrt(3), 0]
    Γ₀ = 2π / 3d₀ * SA[0, 0, 0]
    Y₀ = 2π / 3d₀ * SA[1, 0, 0]
    Γ!₀ = 2π / 3d₀ * SA[1, -sqrt(3), 0]
    M₀ = 2π / 3d₀ * SA[1, -sqrt(3), 0] / 2

    MomentumSpacePath(points, X₀, Γ₀, Y₀, Γ!₀, M₀)
end

function Base.iterate(::MomentumSpacePath)
    (SA[0, 2π*sqrt(3)/3d₀, 0] + OUT_OF_PLANE_MOMENTUM, (:X, 0))
end

function Base.iterate(path::MomentumSpacePath, state)
    section, i = state

    X₀_length = norm(path.Γ₀ - path.X₀)
    Γ₀_length = norm(path.Y₀ - path.Γ₀)
    Y₀_length = norm(path.Γ!₀ - path.Y₀)
    Γ!₀_length = norm(path.M₀ - path.Γ!₀)
    M₀_length = norm(path.Γ₀ - path.M₀)
    length = X₀_length + Γ₀_length + Y₀_length + Γ!₀_length + M₀_length

    X₀_steps = round(Int, path.points * X₀_length / length)
    Γ₀_steps = round(Int, path.points * Γ₀_length / length)
    Y₀_steps = round(Int, path.points * Y₀_length / length)
    Γ!₀_steps = round(Int, path.points * Γ!₀_length / length)
    M₀_steps = round(Int, path.points * M₀_length / length)

    i += 1

    if section === :X && i == X₀_steps
        i = 0
        section = :Γ
    elseif section === :Γ && i == Γ₀_steps
        i = 0
        section = :Y
    elseif section === :Y && i == Y₀_steps
        i = 0
        section = :Γ!
    elseif section === :Γ! && i == Γ!₀_steps
        i = 0
        section = :M
    elseif section === :M && i == M₀_steps
        return nothing
    end

    start, dk = if section === :X
        path.X₀, (path.Γ₀ - path.X₀) / X₀_steps
    elseif section === :Γ
        path.Γ₀, (path.Y₀ - path.Γ₀) / Γ₀_steps
    elseif section === :Y
        path.Y₀, (path.Γ!₀ - path.Y₀) / Y₀_steps
    elseif section === :Γ!
        path.Γ!₀, (path.M₀ - path.Γ!₀) / Γ!₀_steps
    elseif section === :M
        path.M₀, (path.Γ₀ - path.M₀) / M₀_steps
    else
        error("unknown momentum space section")
    end

    k::SVector{3,Float64} = start + i * dk + OUT_OF_PLANE_MOMENTUM
    (k, (section, i))
end

# NOTE: Unknown size because rounding means that the total number of steps isn't necessarily the same as requested number of points.
Base.IteratorSize(::Type{MomentumSpacePath}) = Base.SizeUnknown()
Base.eltype(::Type{MomentumSpacePath}) = SVector{3,Float64}

function symmetry_points(path)
    @debug "Points" X₀ = tuple(path.X₀...) Γ₀ = tuple(path.Γ₀...) Y₀ = tuple(path.Y₀...) Γ!₀ =
        tuple(path.Γ!₀...) M₀ = tuple(path.M₀...)
    unit(vector) = vector / norm(vector)
    between(first, second, point) = unit(point - first) ≈ unit(second - point)

    points = Dict(1 => "X")
    previous = first(path) - OUT_OF_PLANE_MOMENTUM
    previous_direction = unit(path.Γ₀ - path.X₀)
    for (i, current) in Iterators.drop(enumerate(path), 1)
        current -= OUT_OF_PLANE_MOMENTUM

        @debug "Iteration" i current = tuple(current...) previous = tuple(previous...)
        if current == path.M₀ || between(previous, current, path.M₀)
            points[i] = "M"
        end

        current_direction = unit(current - previous)

        if !(current_direction ≈ previous_direction)
            @debug "Direction changed!" i current = tuple(current...) prevdir =
                tuple(previous_direction...) curdir = tuple(current_direction...)
            previous_direction = current_direction

            closest = ("", SA[Inf, Inf, Inf])

            for (key, point) in ["Γ" => path.Γ₀, "Y" => path.Y₀, "Γ'" => path.Γ!₀]
                @debug "Trying" key new = norm(point - current) old =
                    norm(closest[2] - current)
                if norm(point - current) < norm(closest[2] - current)
                    @debug "Closer"
                    closest = (key, point)
                end
            end

            @debug "Picked" closest
            points[i] = closest[1]
        end

        previous = current
    end

    points[last(collect(enumerate(path)))[1]] = "Γ"

    points
end

end  # module
