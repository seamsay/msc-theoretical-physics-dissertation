module Plots

using CairoMakie
using JLD2
using LinearAlgebra
using Meander
using StaticArrays

import ..d₀,
    ..down,
    ..read_spectral_function,
    ..up,
    ..Σ_1,
    ..Σ_2,
    ..E,
    ..E_abnormal,
    ..GMK,
    ..S,
    ..MomentumSpacePath,
    ..Parameters,
    ..Path

function dispersion_bilinear_vs_biquadratic_2d(path = Meander.Path(1000, GMK))
    symmetry_indices, symmetry_labels = if path isa Meander.Path
        indices(path), tags(path)
    else
        symmetry_indices, symmetry_labels =
            zip(sort(collect(Path.symmetry_points(path)))...)
        symmetry_indices = collect(symmetry_indices)
        symmetry_labels = collect(symmetry_labels)
    end

    ks = collect(path)
    xs = collect(eachindex(ks))

    quys = E.(ks, Ref(up), Ref(Parameters()))
    qdys = E.(ks, Ref(down), Ref(Parameters()))
    luys = E.(ks, Ref(up), Ref(Parameters(:bilinear)))
    ldys = E.(ks, Ref(down), Ref(Parameters(:bilinear)))

    figure = Figure()
    figure[1, 1] =
        Axis(figure, ylabel = "Energy (meV)", xticks = (symmetry_indices, symmetry_labels))

    q_plot = lines!(figure[1, 1], xs, quys, color = :red, linestyle = :dot)
    lines!(figure[1, 1], xs, qdys, color = :red, linestyle = :dot)
    l_plot = lines!(figure[1, 1], xs, luys, color = :blue, linestyle = :dash)
    lines!(figure[1, 1], xs, ldys, color = :blue, linestyle = :dash)

    figure[1, 1] = Legend(
        figure,
        [l_plot, q_plot],
        ["Bilinear", "Biquadratic"],
        tellwidth = false,
        halign = :right,
        valign = :top,
        # TODO: Is there a better way to get the `halign` correct than abusing `margin`?
        margin = (10, 155, 10, 10),
    )

    figure
end

function dispersion_normal_vs_abnormal_2d(path = Meander.Path(1000, GMK))
    symmetry_indices, symmetry_labels = if path isa Meander.Path
        indices(path), tags(path)
    else
        symmetry_indices, symmetry_labels =
            zip(sort(collect(Path.symmetry_points(path)))...)
        symmetry_indices = collect(symmetry_indices)
        symmetry_labels = collect(symmetry_labels)
    end

    ks = collect(path)
    xs = collect(eachindex(ks))

    nuys = E.(ks, Ref(up), Ref(Parameters()))
    ndys = E.(ks, Ref(down), Ref(Parameters()))
    auys = E_abnormal.(ks, Ref(up), Ref(Parameters()))
    adys = E_abnormal.(ks, Ref(down), Ref(Parameters()))

    figure = Figure()
    figure[1, 1] =
        Axis(figure, ylabel = "Energy (meV)", xticks = (symmetry_indices, symmetry_labels))

    n_plot = lines!(figure[1, 1], xs, nuys, color = :red, linestyle = :dot)
    lines!(figure[1, 1], xs, ndys, color = :red, linestyle = :dot)
    a_plot = lines!(figure[1, 1], xs, auys, color = :blue, linestyle = :dash)
    lines!(figure[1, 1], xs, adys, color = :blue, linestyle = :dash)

    figure[1, 1] = Legend(
        figure,
        [a_plot, n_plot],
        ["Before Normal-Order", "After Normal-Order"],
        tellwidth = false,
        halign = :right,
        valign = :top,
        # TODO: Is there a better way to get the `halign` correct than abusing `margin`?
        margin = (10, 155, 10, 10),
    )

    figure
end

function dispersion3d(k₁ = -2π/3d₀:0.01:2π/3d₀, k₂ = -π/sqrt(3)d₀:0.01:π/sqrt(3)d₀)
    figure = Figure()
    axis =
        figure[1, 1] = Axis3(
            figure,
            xlabel = "k₁",
            ylabel = "k₂",
            zlabel = "Energy (meV)",
            # TODO: Figure out which  of these is needed, I literally just copied it wholesale from https://lazarusa.github.io/BeautifulMakie/surfWireLines/surfaceConstraints/.
            aspect = (1, 1, 1),
            perspectiveness = 0.5,
            elevation = π / 9,
            azimuth = 0.2π,
        )
    hidexdecorations!(axis, label = false, grid = false)
    hideydecorations!(axis, label = false, grid = false)

    surface!(
        axis,
        k₁,
        k₂,
        [E(SA[x, y, 0], down, Parameters()) for x in k₁, y in k₂],
        colormap = [:darkred, :red],
    )
    surface!(
        axis,
        k₁,
        k₂,
        [E(SA[x, y, 0], up, Parameters()) for x in k₁, y in k₂],
        colormap = [:blue, :darkblue],
    )

    figure
end

function self_energy_1(band, temperature = 0.0:0.1:0.5, path = Meander.Path(100, GMK))
    @assert length(temperature) == 6

    figure = Figure()

    axes = Matrix(undef, 3, 2)
    for (i, T) in enumerate(temperature)
        (row, column) = map(x -> x + 1, divrem(i - 1, 2))
        axes[row, column] =
            figure[row, column] = Axis(figure, xticks = (indices(path), tags(path)))

        if row != 3
            hidexdecorations!(axes[row, column], grid = false)
        end

        if column != 1
            hideydecorations!(axes[row, column], grid = false)
        end

        ys =
            sum.(
                diag.(
                    first.(
                        Σ_1.(
                            path,
                            E.(path, Ref(band), Ref(Parameters())),
                            Ref(T),
                            Ref(Parameters()),
                        ),
                    ),
                ),
            )
        r_plot = lines!(axes[row, column], real.(ys), color = :red)
        i_plot = lines!(axes[row, column], imag.(ys), color = :blue)

        if row == 1 && column == 1
            figure[row, column] = Legend(
                figure,
                [r_plot, i_plot],
                ["Real", "Imaginary"],
                tellwidth = false,
                halign = :left,
                valign = band == down ? :top : :bottom,
                margin = (10, 10, 10, 10),
            )
        end
    end

    for (left, right) in eachrow(axes)
        linkyaxes!(left, right)
    end

    for (top, middle, bottom) in eachcol(axes)
        linkxaxes!(bottom, middle, top)
    end

    axes[2, 1].ylabel = "Self-Energy (meV)"

    figure
end

function self_energy_1_single(band, T = 5.0, path = Meander.Path(100, GMK))
    figure = Figure()
    figure[1, 1] =
        Axis(figure, ylabel = "Self-Energy (meV)", xticks = (indices(path), tags(path)))

    ys =
        sum.(
            diag.(
                first.(
                    Σ_1.(
                        path,
                        E.(path, Ref(band), Ref(Parameters())),
                        Ref(T),
                        Ref(Parameters()),
                    ),
                ),
            ),
        )
    r_plot = lines!(figure[1, 1], real.(ys), color = :red)
    i_plot = lines!(figure[1, 1], imag.(ys), color = :blue)

    figure[1, 1] = Legend(
        figure,
        [r_plot, i_plot],
        ["Real", "Imaginary"],
        tellwidth = false,
        halign = band == down ? :center : :left,
        valign = band == down ? :top : :bottom,
        margin = (10, 10, 10, 10),
    )

    figure
end

function self_energy_2(band, temperature = 0.0:0.1:0.5, path = Meander.Path(100, GMK))
    @assert length(temperature) == 6

    figure = Figure()

    axes = Matrix(undef, 3, 2)
    for (i, T) in enumerate(temperature)
        (row, column) = map(x -> x + 1, divrem(i - 1, 2))
        axes[row, column] =
            figure[row, column] = Axis(figure, xticks = (indices(path), tags(path)))

        if row != 3
            hidexdecorations!(axes[row, column], grid = false)
        end

        if column != 1
            hideydecorations!(axes[row, column], grid = false)
        end

        ys =
            sum.(
                diag.(
                    first.(
                        Σ_2.(
                            path,
                            E.(path, Ref(band), Ref(Parameters())),
                            Ref(T),
                            Ref(Parameters()),
                        ),
                    ),
                ),
            )
        r_plot = lines!(axes[row, column], real.(ys), color = :red)
        i_plot = lines!(axes[row, column], imag.(ys), color = :blue)

        if row == 1 && column == 1
            figure[row, column] = Legend(
                figure,
                [r_plot, i_plot],
                ["Real", "Imaginary"],
                tellwidth = false,
                halign = :left,
                valign = :bottom,
                margin = (10, 10, 10, 10),
            )
        end
    end

    for (left, right) in eachrow(axes)
        linkyaxes!(left, right)
    end

    for (top, middle, bottom) in eachcol(axes)
        linkxaxes!(bottom, middle, top)
    end

    axes[2, 1].ylabel = "Self-Energy (meV)"

    figure
end

function self_energy_2_single(band, T = 5.0, path = Meander.Path(100, GMK))
    figure = Figure()
    figure[1, 1] =
        Axis(figure, ylabel = "Self-Energy (meV)", xticks = (indices(path), tags(path)))

    ys =
        sum.(
            diag.(
                first.(
                    Σ_2.(
                        path,
                        E.(path, Ref(band), Ref(Parameters())),
                        Ref(T),
                        Ref(Parameters()),
                    ),
                ),
            ),
        )
    r_plot = lines!(figure[1, 1], real.(ys), color = :red)
    i_plot = lines!(figure[1, 1], imag.(ys), color = :blue)

    figure[1, 1] = Legend(
        figure,
        [r_plot, i_plot],
        ["Real", "Imaginary"],
        tellwidth = false,
        halign = band == down ? :center : :left,
        valign = :top,
        margin = (10, 10, 10, 10),
    )

    figure
end

function spectral_function_1st_2nd_6(
    temperatures = 0.0:0.1:0.5,
    path = Meander.Path(20, GMK),
    energies = 0.0:0.5:50,
)
    @assert length(temperatures) == 6

    figure = Figure()
    axes = Matrix(undef, 3, 2)
    heatmaps = Matrix(undef, 3, 2)
    for (i, T) in enumerate(temperatures)
        (x, y) = map(x -> x + 1, divrem(i - 1, 2))

        data = load(
            joinpath(
                "data",
                "spectral_function-1st_2nd_order-$(join(tags(path), '_'))-Nk_$(length(path))-Nω_$(length(energies))-T_$T.jld2",
            ),
        )

        @assert collect(data["path"]) == collect(path)
        @assert data["energies"] == energies
        reduction = 1:31
        reduced_energies = energies[reduction]
        reduced_spectral = data["spectral"][:, reduction]

        axes[x, y] =
            axis = figure[x, y] = Axis(figure, xticks = (indices(path), tags(path)))

        if x != 3
            hidexdecorations!(axis)
        end

        if y != 1
            hideydecorations!(axis)
        end

        bounds = (0.0, 0.5)
        heatmaps[x, y] = heatmap!(
            figure[x, y],
            1:length(path),
            reduced_energies,
            clamp.(reduced_spectral, bounds...),
            colorrange = bounds,
        )
    end

    axes[2, 1].ylabel = "Energy (meV)"

    for (left, right) in eachrow(axes)
        linkyaxes!(left, right)
    end

    for (top, middle, bottom) in eachcol(axes)
        linkxaxes!(top, middle, bottom)
    end

    figure[:, 3] = Colorbar(
        figure,
        heatmaps[1, 1],
        height = Relative(9 / 10),
        label = "Spectral Function (meV⁻¹)",
    )

    figure
end

function spectral_function_1st_2nd(
    T = 2.0,
    path = Meander.Path(20, GMK),
    energies = 0.0:0.5:50;
    reduction = 1:61,
    upper = 0.02,
)
    figure = Figure()

    data = load(
        joinpath(
            "data",
            "spectral_function-1st_2nd_order-$(join(tags(path), '_'))-Nk_$(length(path))-Nω_$(length(energies))-T_$T.jld2",
        ),
    )

    @assert collect(data["path"]) == collect(path)
    @assert data["energies"] == energies
    reduced_energies = energies[reduction]
    reduced_spectral = data["spectral"][:, reduction]

    figure[1, 1] =
        Axis(figure, ylabel = "Energy (meV)", xticks = (indices(path), tags(path)))

    bounds = (0.0, upper)
    h_plot = heatmap!(
        figure[1, 1],
        1:length(path),
        reduced_energies,
        clamp.(reduced_spectral, bounds...),
        colorrange = bounds,
    )

    figure[:, 2] = Colorbar(
        figure,
        h_plot,
        height = Relative(9 / 10),
        label = "Spectral Function (meV⁻¹)",
    )

    figure
end

function generate(target)
    @sync begin
        @async save(
            joinpath(target, "dispersion-bilinear_vs_biquadratic-2d.png"),
            dispersion_bilinear_vs_biquadratic_2d(),
        )
        @async save(
            joinpath(target, "dispersion-pre_vs_post_normal-2d.png"),
            dispersion_normal_vs_abnormal_2d(),
        )

        @async let k₁ = 4π * sqrt(3) / 9, k₂ = 0.0, width = 0.1, step = 0.01
            save(
                joinpath(target, "dispersion-dirac_cone.png"),
                dispersion3d((k₁-width):step:(k₁+width), (k₂-width):step:(k₂+width)),
            )
        end

        @async save(
            joinpath(target, "spectral_function-T_0.0.png"),
            spectral_function_1st_2nd(
                0.0,
                Meander.Path(1000, GMK),
                0.0:0.1:40.0,
                reduction = 1:221,
                upper = 1.0,
            ),
        )

        @async save(
            joinpath(target, "spectral_function-T_0.0_0.5.png"),
            spectral_function_1st_2nd_6(),
        )

        @async save(
            joinpath(target, "spectral_function-T_2.0.png"),
            spectral_function_1st_2nd()
        )
    end
end

end  # module
