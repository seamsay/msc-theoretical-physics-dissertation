include("Path.jl")
using .Path

import Meander

using Cuba
using JLD2
using LinearAlgebra
using StaticArrays

const KHG = [
    "X" => 2π / 3d₀ * SA[-sqrt(3), 0.0, 0.0] + Path.OUT_OF_PLANE_MOMENTUM,
    "K'" => 2π / 3d₀ * SA[-2sqrt(3)/3, 0.0, 0.0] + Path.OUT_OF_PLANE_MOMENTUM,
    "Γ" => 2π / 3d₀ * SA[0.0, 0.0, 0.0] + Path.OUT_OF_PLANE_MOMENTUM,
    "Y" => 2π / 3d₀ * SA[0.0, 1.0, 0.0] + Path.OUT_OF_PLANE_MOMENTUM,
    "K" => 2π / 3d₀ * SA[sqrt(3)/3, 1.0, 0.0] + Path.OUT_OF_PLANE_MOMENTUM,
    "Γ'" => 2π / 3d₀ * SA[sqrt(3), 1.0, 0.0] + Path.OUT_OF_PLANE_MOMENTUM,
    "M" => 2π / 3d₀ * SA[sqrt(3), 1.0, 0.0] / 2 + Path.OUT_OF_PLANE_MOMENTUM,
    "Γ" => 2π / 3d₀ * SA[0.0, 0.0, 0.0] + Path.OUT_OF_PLANE_MOMENTUM,
]

const GMK = [
    "Γ" => SA[0.0, 0.0, 0.0] + Path.OUT_OF_PLANE_MOMENTUM,
    "M" => 2π / 3d₀ * SA[sqrt(3)/2, 1.0/2, 0.0] + Path.OUT_OF_PLANE_MOMENTUM,
    "K" => 2π / 3d₀ * SA[2sqrt(3)/3, 0.0, 0.0] + Path.OUT_OF_PLANE_MOMENTUM,
    "Γ" => SA[0.0, 0.0, 0.0] + Path.OUT_OF_PLANE_MOMENTUM,
]

export down, up, E, MomentumSpacePath, Parameters

struct Parameters
    S::Float64
    D::Float64
    D_BQ::Float64
    J::Float64
    λ::Float64
    K::Float64
end

Parameters() = Parameters(:biquadratic)
Parameters(key::Symbol) =
    if key === :bilinear
        Parameters(1.5, 0.10882, 0.0, 2.01, 0.1068, 0.0)
    elseif key === :biquadratic
        Parameters(1.5, 0.10882, 0.01, 2.01, 0.1068, 0.22)
    else
        throw(KeyError(key))
    end

@enum Band up = 1 down = -1

const NEAREST_NEIGHBOURS = SA[
    d₀*SA[0.0, 1.0, 0.0],
    d₀*let θ = -30
        SA[cos(2π * θ / 360), sin(2π * θ / 360), 0.0]
    end,
    d₀*let θ = -150
        SA[cos(2π * θ / 360), sin(2π * θ / 360), 0.0]
    end,
]

const BRILLOUIN_ZONE_AREA = (2π / 3d₀)^2 * sqrt(3)

const UNIT_CELL_SITES = 2

E(k, band, parameters) =
    let S = parameters.S,
        D = parameters.D,
        D_BQ = parameters.D_BQ,
        J = parameters.J,
        λ = parameters.λ,
        K = parameters.K,
        b = Int(band)
        #! format: off
        (2S - 1) * D           +
        2D_BQ * S^2 * (2S - 3) +
        3S * (J + λ)           +
        6K * S^2 * (S - 1)     +
        b * (
            J * S +
            2K * S^2 * (S - 1)
        ) * abs(γ(k))
        #! format: on
    end

E_abnormal(k, band, parameters) =
    let S = parameters.S,
        D = parameters.D,
        D_BQ = parameters.D_BQ,
        J = parameters.J,
        λ = parameters.λ,
        K = parameters.K,
        b = Int(band)
        #! format: off
        2D * S       +
        4D_BQ * S^3  +
        3S * (J + λ) +
        6K * S^3     +
        b * (
            J * S +
            2K * S^3
        ) * abs(γ(k))
        #! format: on
    end

M(k, p) = SA[
    E(k, down, p) 0.0
    0.0 E(k, up, p)
]

const δ = 1e-3

G(k, ω, T, parameters) =
    (
        (ω + δ * im) * I - M(k, parameters) - Σ_1(k, ω, T, parameters)[1] -
        Σ_2(k, ω, T, parameters)[1]
    )^-1

A(k, ω, T, parameters) = -1 * (1 - n_B(ω, T)) * imag(sum(diag(G(k, ω, T, parameters)))) / π

S(k, ω, T, parameters) = -1 * (1 - n_B(ω, T)) * imag(G(k, ω, T, parameters)) / π

γ(k) =
    let τ1 = NEAREST_NEIGHBOURS[1], τ2 = NEAREST_NEIGHBOURS[2], τ3 = NEAREST_NEIGHBOURS[3]
        θ1 = k ⋅ τ1
        θ2 = k ⋅ τ2
        θ3 = k ⋅ τ3

        cos(θ1) + cos(θ2) + cos(θ3) + im * (sin(θ1) + sin(θ2) + sin(θ3))
    end

ξ(k) = angle(γ(k))

n_B(k, band, T, parameters) = n_B(E(k, band, parameters), T)

n_B(E, T) = (exp(E / T) - 1)^-1

include("vertices.jl")

V_1121(k, p, q, parameters) = V_1112(p, k, q, parameters)

V_2111(k, p, q, parameters) = V_1211(k, p, k + p - q, parameters)

V_1221(k, p, q, parameters) = V_1212(p, k, q, parameters)
V_2121(k, p, q, parameters) = V_1212(p, k, k + p - q, parameters)
V_2112(k, p, q, parameters) = V_1212(k, p, k + p - q, parameters)

V_2122(k, p, q, parameters) = V_1222(k, p, k + p - q, parameters)

V_2221(k, p, q, parameters) = V_2212(p, k, q, parameters)

function Σ_1(k, ω, T, parameters)
    function momenta(x)
        (a1, a2) = x

        left = -sqrt(3) * π / 3d₀
        right = sqrt(3) * π / 3d₀
        bottom = -π / 3d₀
        top = π / 3d₀

        p1 = left + a1 * (right - left)
        p2 = bottom + a2 * (top - bottom)

        p = SA[p1, p2, 0] + Path.OUT_OF_PLANE_MOMENTUM
        q = p

        p, q
    end

    function integrand(x, output, down_vertex, up_vertex)
        p, q = momenta(x)

        result = let E_p = E(p, down, parameters)
            n_B(E_p, T) * down_vertex(k, p, q, parameters) / (ω + δ * im - E_p)
        end

        result += let E_p = E(p, up, parameters)
            n_B(E_p, T) * up_vertex(k, p, q, parameters) / (ω + δ * im - E_p)
        end

        output[1] = real(result)
        output[2] = imag(result)
    end

    # TODO: Make V a tensor?
    i11 = cuhre((x, output) -> integrand(x, output, V_1111, V_1221), 2, 2)
    i12 = cuhre((x, output) -> integrand(x, output, V_1112, V_1222), 2, 2)
    i21 = cuhre((x, output) -> integrand(x, output, V_2111, V_2221), 2, 2)
    i22 = cuhre((x, output) -> integrand(x, output, V_2112, V_2222), 2, 2)

    integral = SA[
        i11.integral[1]+i11.integral[2]*im i12.integral[1]+i12.integral[2]*im
        i21.integral[1]+i21.integral[2]*im i22.integral[1]+i22.integral[2]*im
    ]

    (24 * integral / (BRILLOUIN_ZONE_AREA * UNIT_CELL_SITES), [i11 i12; i21 i22])
end

function Σ_2(k, ω, T, parameters)
    function momenta(x)
        (a1, a2, b1, b2) = x

        left = -sqrt(3) * π / 3d₀
        right = sqrt(3) * π / 3d₀
        bottom = -π / 3d₀
        top = π / 3d₀

        p1 = left + a1 * (right - left)
        p2 = bottom + a2 * (top - bottom)
        q1 = left + b1 * (right - left)
        q2 = bottom + b2 * (top - bottom)

        p = SA[p1, p2, 0] + Path.OUT_OF_PLANE_MOMENTUM
        q = SA[q1, q2, 0] + Path.OUT_OF_PLANE_MOMENTUM

        p, q
    end

    function integrand(
        x,
        output,
        ddd_a,
        ddd_c,
        ddu_a,
        ddu_c,
        dud_a,
        dud_c,
        duu_a,
        duu_c,
        udd_a,
        udd_c,
        udu_a,
        udu_c,
        uud_a,
        uud_c,
        uuu_a,
        uuu_c,
    )
        (p, q) = momenta(x)

        function summand(E_p, E_q, E_kpq, annihilation, creation)
            f_p = n_B(E_p, T)
            f_q = n_B(E_q, T)
            f_kpq = n_B(E_kpq, T)

            F = (1 + f_q) * (1 + f_kpq) * f_p - f_q * f_kpq * (1 + f_p)

            F * annihilation(k, p, q, parameters) * creation(k + p - q, q, p, parameters) /
            (ω + δ * im + E_p - E_q - E_kpq)
        end

        result = summand(
            E(p, down, parameters),
            E(q, down, parameters),
            E(k + p - q, down, parameters),
            ddd_a,
            ddd_c,
        )
        result += summand(
            E(p, down, parameters),
            E(q, down, parameters),
            E(k + p - q, up, parameters),
            ddu_a,
            ddu_c,
        )
        result += summand(
            E(p, down, parameters),
            E(q, up, parameters),
            E(k + p - q, down, parameters),
            dud_a,
            dud_c,
        )
        result += summand(
            E(p, down, parameters),
            E(q, up, parameters),
            E(k + p - q, up, parameters),
            duu_a,
            duu_c,
        )
        result += summand(
            E(p, up, parameters),
            E(q, down, parameters),
            E(k + p - q, down, parameters),
            udd_a,
            udd_c,
        )
        result += summand(
            E(p, up, parameters),
            E(q, down, parameters),
            E(k + p - q, up, parameters),
            udu_a,
            udu_c,
        )
        result += summand(
            E(p, up, parameters),
            E(q, up, parameters),
            E(k + p - q, down, parameters),
            uud_a,
            uud_c,
        )
        result += summand(
            E(p, up, parameters),
            E(q, up, parameters),
            E(k + p - q, up, parameters),
            uuu_a,
            uuu_c,
        )

        # TODO: Are we getting bad results because of singularities? If so, can we use http://www.johndcook.com/blog/2012/02/21/care-and-treatment-of-singularities/ ?
        # TODO: Plot out the integrand.
        # TODO: Plot out each of the vertices.
        output[1] = real(result)
        output[2] = imag(result)
    end

    # TODO: Maybe start with divonne at ~500_000 maxevals, then fall back to cuhre?
    i11 = cuhre(
        (x, output) -> integrand(
            x,
            output,
            V_1111,
            V_1111,
            V_2111,
            V_1112,
            V_1211,
            V_1121,
            V_2211,
            V_1122,
            V_1121,
            V_1211,
            V_2121,
            V_1212,
            V_1221,
            V_1221,
            V_2221,
            V_1222,
        ),
        4,
        2,
    )
    i12 = cuhre(
        (x, output) -> integrand(
            x,
            output,
            V_1112,
            V_1111,
            V_2112,
            V_1112,
            V_1212,
            V_1121,
            V_2212,
            V_1122,
            V_1122,
            V_1211,
            V_2122,
            V_1212,
            V_1222,
            V_1221,
            V_2222,
            V_1222,
        ),
        4,
        2,
    )
    i21 = cuhre(
        (x, output) -> integrand(
            x,
            output,
            V_1111,
            V_2111,
            V_2111,
            V_2112,
            V_1211,
            V_2121,
            V_2211,
            V_2122,
            V_1121,
            V_2211,
            V_2121,
            V_2212,
            V_1221,
            V_2221,
            V_2221,
            V_2222,
        ),
        4,
        2,
    )
    i22 = cuhre(
        (x, output) -> integrand(
            x,
            output,
            V_1112,
            V_2111,
            V_2112,
            V_2112,
            V_1212,
            V_2121,
            V_2212,
            V_2122,
            V_1122,
            V_2211,
            V_2122,
            V_2212,
            V_1222,
            V_2221,
            V_2222,
            V_2222,
        ),
        4,
        2,
    )

    integral = SA[
        i11.integral[1]+i11.integral[2]*im i12.integral[1]+i12.integral[2]*im
        i21.integral[1]+i21.integral[2]*im i22.integral[1]+i22.integral[2]*im
    ]

    (24^2 * integral / (2 * (BRILLOUIN_ZONE_AREA * UNIT_CELL_SITES)^2), [i11 i12; i21 i22])
end

function calculate_spectral_function(
    T,
    path = Meander.Path(20, GMK),
    energies = 0.0:0.2:50,
    parameters = Parameters(),
)
    directory = "data"
    if !ispath(directory)
        mkpath(directory)
    elseif !isdir(directory)
        error("$directory is not a directory!")
    end
    filepath = joinpath(
        directory,
        "spectral_function-1st_2nd_order-$(join(Meander.tags(path), '_'))-Nk_$(length(path))-Nω_$(length(energies))-T_$(T).jld2",
    )

    spectral = [A(k, ω, T, parameters) for k in path, ω in energies]

    jldsave(filepath; path, energies, spectral)
end

struct SpectralFunction
    ks::Vector{SVector{3,Float64}}
    ωs::Vector{Float64}
    As::Matrix{Float64}

    function SpectralFunction(ks, ωs, As)
        @assert size(As) == (length(ks), length(ωs))
        new(ks, ωs, As)
    end
end

# TODO: There must be a much better way... Maybe use HDF5 instead of a TSV file.
function read_spectral_function(path)
    k_indices_down = Int[]
    k_indices_up = Int[]
    ks_down = SVector{3,Float64}[]
    ks_up = SVector{3,Float64}[]
    ω_indices_down = Int[]
    ω_indices_up = Int[]
    ωs_down = Float64[]
    ωs_up = Float64[]
    As_down = Float64[]
    As_up = Float64[]

    open(path) do file
        readline(file)

        while !eof(file)
            line = split(readline(file))

            band = line[1]
            k_index = parse(Int, line[2])
            k = SA[parse.(Float64, line[3:5])...]
            ω_index = parse(Int, line[6])
            ω = parse(Float64, line[7])
            A = parse(Float64, line[8])

            if band == "down"
                push!(k_indices_down, k_index)
                push!(ks_down, k)
                push!(ω_indices_down, ω_index)
                push!(ωs_down, ω)
                push!(As_down, A)
            elseif band == "up"
                push!(k_indices_up, k_index)
                push!(ks_up, k)
                push!(ω_indices_up, ω_index)
                push!(ωs_up, ω)
                push!(As_up, A)
            else
                error("Unknown band `$band`.")
            end
        end
    end

    n_ks_down = maximum(k_indices_down)
    n_ωs_down = maximum(ω_indices_down)
    ks_down_final = fill(SA[NaN, NaN, NaN], n_ks_down)
    ωs_down_final = fill(NaN, n_ωs_down)
    As_down_final = Matrix{Float64}(undef, n_ks_down, n_ωs_down)
    for (ki, k, ωi, ω, A) in zip(k_indices_down, ks_down, ω_indices_down, ωs_down, As_down)
        if all(isnan, ks_down_final[ki])
            ks_down_final[ki] = k
        elseif ks_down_final[ki] != k
            error("k mismatch at index $ki: $(ks_down_final[ki]) != $k")
        end

        if isnan(ωs_down_final[ωi])
            ωs_down_final[ωi] = ω
        elseif ωs_down_final[ωi] != ω
            error("ω mismatch at index $ωi: $(ωs_down_final[ωi]) != $ω")
        end

        As_down_final[ki, ωi] = A
    end

    n_ks_up = maximum(k_indices_up)
    n_ωs_up = maximum(ω_indices_up)
    ks_up_final = fill(SA[NaN, NaN, NaN], n_ks_up)
    ωs_up_final = fill(NaN, n_ωs_up)
    As_up_final = Matrix{Float64}(undef, n_ks_up, n_ωs_up)
    for (ki, k, ωi, ω, A) in zip(k_indices_up, ks_up, ω_indices_up, ωs_up, As_up)
        if all(isnan, ks_up_final[ki])
            ks_up_final[ki] = k
        elseif ks_up_final[ki] != k
            error("k mismatch at index $ki: $(ks_up_final[ki]) != $k")
        end

        if isnan(ωs_up_final[ωi])
            ωs_up_final[ωi] = ω
        elseif ωs_up_final[ωi] != ω
            error("ω mismatch at index $ωi: $(ωs_up_final[ωi]) != $ω")
        end

        As_up_final[ki, ωi] = A
    end

    @assert ks_down_final == ks_up_final
    @assert ωs_down_final == ωs_up_final

    SpectralFunction(ks_down_final, ωs_down_final, As_down_final),
    SpectralFunction(ks_up_final, ωs_up_final, As_up_final)
end
