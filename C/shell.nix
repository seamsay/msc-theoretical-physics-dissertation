let pkgs = import <nixpkgs> { };
in pkgs.mkShell {
  name = "msc-dissertation";
  packages = with pkgs; [ bintools clang gcc ];
}
