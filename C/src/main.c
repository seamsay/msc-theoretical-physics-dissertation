// CAUTION: This code is not correct, see the Julia code in the main directory.
#include "cuba.h"
#include <assert.h>
#include <complex.h>
#include <math.h>
#include <stdio.h>

// Real and imaginary.
#define COMPONENTS 2

// TODO: Put this in it's own file.
typedef struct {
  double x;
  double y;
  double z;
} Vector;

typedef struct {
  double S;
  double D;
  double D_BQ;
  double J;
  double lambda;
  double K;
} Parameters;

Parameters biquadratic_default() {
  Parameters p = {
      .S = 1.5,
      .D = 0.10882,
      .D_BQ = 0.01,
      .J = 2.01,
      .lambda = 0.1068,
      .K = 0.22,
  };

  return p;
}

typedef enum {
  up = 1,
  down = -1,
} Band;

double dot(Vector l, Vector r) { return l.x * r.x + l.y * r.y + l.z * r.z; }

double complex gammav(Vector k) {
  Vector tau_1 = {0.0, 1.0, 0.0};
  // TODO: Are these done at compile time?
  Vector tau_2 = {cos(-30 * 2 * M_PI / 360), sin(-30 * 2 * M_PI / 360), 0.0};
  Vector tau_3 = {cos(-150 * 2 * M_PI / 360), sin(-150 * 2 * M_PI / 360), 0.0};

  double theta_1 = dot(k, tau_1);
  double theta_2 = dot(k, tau_2);
  double theta_3 = dot(k, tau_3);

  return cexp(theta_1 * I) + cexp(theta_2 * I) + cexp(theta_3 * I);
}

double E(Vector k, Band band, Parameters p) {
  // clang-format off
  return (2*p.S - 1) * p.D / 2              +
         2 * p.D_BQ * p.S * p.S * (p.S - 3) +
         3 * p.S * (p.J + p.lambda) / 2     +
         3 * p.K * p.S * p.S * (p.S - 1)    +
         band * (
           p.J * p.S / 2 +
           p.K * p.S * p.S * (p.S - 1)
         ) * cabs(gammav(k));
  // clang-format on
}

double xi(Vector k) { return carg(gammav(k)); }

Vector add(Vector l, Vector r) {
  Vector v = {l.x + r.x, l.y + r.y, l.z + r.z};
  return v;
}

Vector sub(Vector l, Vector r) {
  Vector v = {l.x - r.x, l.y - r.y, l.z - r.z};
  return v;
}

Vector neg(Vector v) {
  Vector u = {-v.x, -v.y, -v.z};
  return u;
}

Vector kpq(Vector k, Vector p, Vector q) { return add(k, sub(p, q)); }

// TODO: Give this a header file and link it properly.
#include "vertices.c"

double f(double energy, double temperature) {
  return 1.0 / (exp(energy / temperature) - 1);
}

typedef struct {
  Vector k;
  double omega;
  double T;
  Band band;
  Parameters pa;
} UserData;

typedef struct {
  double complex integral;
  double complex error;
  double complex probability;
  int regions;
  int evaluations;
  int failure;
} Integral;

int sigma_2_integrand(const int *ndim, const double x[4], const int *ncomp,
                      double output[COMPONENTS], void *userdata_) {
  assert(*ndim == 4);
  assert(*ncomp == COMPONENTS);

  UserData userdata = *((UserData *)userdata_);

  double a1 = x[0];
  double a2 = x[1];
  double b1 = x[2];
  double b2 = x[3];

  double left = -M_PI / 3.0;
  double right = M_PI / 3.0;
  double bottom = -sqrt(3.0) * M_PI / 3.0;
  double top = sqrt(3.0) * M_PI / 3.0;

  Vector p = {left + a1 * (right - left), bottom + a2 * (top - bottom), 0.0};
  Vector q = {left + b1 * (right - left), bottom + b2 * (top - bottom), 0.0};

  double E_p = E(p, userdata.band, userdata.pa);
  double E_q = E(q, userdata.band, userdata.pa);
  double E_kpq = E(kpq(userdata.k, p, q), userdata.band, userdata.pa);

  double f_p = f(E_p, userdata.T);
  double f_q = f(E_q, userdata.T);
  double f_kpq = f(E_kpq, userdata.T);

  double F = (1 + f_q) * (1 + f_kpq) * f_p - f_q * f_kpq * (1 + f_p);

  double abs_v_1111 = V_1111(userdata.k, p, q, userdata.pa);
  double abs_v_1112 = V_1112(userdata.k, p, q, userdata.pa);
  double abs_v_1122 = V_1122(userdata.k, p, q, userdata.pa);
  double abs_v_1211 = V_1211(userdata.k, p, q, userdata.pa);
  double abs_v_1212 = V_1212(userdata.k, p, q, userdata.pa);
  double abs_v_1222 = V_1222(userdata.k, p, q, userdata.pa);
  double abs_v_2211 = V_2211(userdata.k, p, q, userdata.pa);
  double abs_v_2212 = V_2212(userdata.k, p, q, userdata.pa);
  double abs_v_2222 = V_2222(userdata.k, p, q, userdata.pa);

  double complex value = (abs_v_1111 * abs_v_1111 + abs_v_1112 * abs_v_1112 +
                          abs_v_1122 * abs_v_1122 + abs_v_1211 * abs_v_1211 +
                          abs_v_1212 * abs_v_1212 + abs_v_1222 * abs_v_1222 +
                          abs_v_2211 * abs_v_2211 + abs_v_2212 * abs_v_2212 +
                          abs_v_2222 * abs_v_2222) *
                         F / (userdata.omega + 1e-3 * I + E_p - E_q - E_kpq);

  output[0] = creal(value);
  output[1] = cimag(value);

  return 0;
}

Integral sigma_2(UserData userdata) {
  int regions = 0;
  int evaluations = 0;
  int failure = 0;

  double integral[COMPONENTS] = {0.0};
  double error[COMPONENTS] = {0.0};
  double probability[COMPONENTS] = {0.0};

  Cuhre(4, // Integral dimension (ndim).
        COMPONENTS, sigma_2_integrand, &userdata,
        1,       // Number of points per itegrand evaluation (nvec).
        1e-4,    // Relative accuracy (epsrel).
        1e-12,   // Absolute accuracy (epsabs).
        0,       // Integration flags (flags).
        0,       // Minimum integrand evaluations (minevals).
        1000000, // Maximum integrand evaluations (maxevals).
        0,       // Polynomial degree (key).
        NULL,    // State file path (statefile).
        NULL,    // Spinning cores pointer (spin).
        &regions, &evaluations, &failure, integral, error, probability);

  Integral i = {
      .integral = integral[0] + integral[1] * I,
      .error = error[0] + error[1] * I,
      .probability = probability[0] + probability[1] * I,
      .regions = regions,
      .evaluations = evaluations,
      .failure = failure,
  };

  return i;
}

int main() {
  UserData userdata = {
      .k = {0.0, 0.0, 0.0},
      .omega = 10.0,
      .T = 2.0,
      .band = down,
      .pa = biquadratic_default(),
  };

  Integral i = sigma_2(userdata);

  printf("%f + %fi\n", creal(i.integral), cimag(i.integral));
  printf("%f + %fi\n", creal(i.error), cimag(i.error));
  printf("%f + %fi\n", creal(i.probability), cimag(i.probability));
  printf("%d\n", i.regions);
  printf("%d\n", i.evaluations);
  printf("%d\n", i.failure);

  return 0;
}
