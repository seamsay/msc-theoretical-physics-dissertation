// TODO: Give this a header file and link it properly.

double complex V_1111(Vector k, Vector p, Vector q, Parameters pa) {
  double xi_k = xi(k);
  double xi_p = xi(p);
  double xi_q = xi(q);
  double xi_kpq = xi(kpq(k, p, q));

  // clang-format off
  return - (pa.D / 8.0 + 3 * pa.D_BQ * pa.S * pa.S / 4.0 + 3 * pa.K * pa.S * pa.S / 8.0)                          * cexp( (xi_k + xi_p - xi_q - xi_kpq) * I / 2.0) +
         - (pa.J / 32.0 + 5 * pa.K * pa.S * pa.S / 16.0)                                 * gammav(k)              * cexp( (xi_p - xi_k - xi_q - xi_kpq) * I / 2.0) +
         -  pa.K * pa.S * pa.S / 8.0                                                     * gammav(add(k, p))      * cexp(-(xi_k + xi_p + xi_q + xi_kpq) * I / 2.0) +
         - (pa.J / 32.0 + 5 * pa.K * pa.S * pa.S / 16.0)                                 * gammav(neg(q))         * cexp( (xi_k + xi_p + xi_q - xi_kpq) * I / 2.0) +
         - (pa.J / 8.0 + pa.lambda / 8.0 + 3 * pa.K * pa.S * pa.S / 4.0)                 * gammav(sub(k, q))      * cexp( (xi_p + xi_q - xi_k - xi_kpq) * I / 2.0) +
         - (pa.J / 32.0 + 5 * pa.K * pa.S * pa.S / 16.0)                                 * gammav(kpq(k, p, q))   * cexp( (xi_q - xi_k - xi_p - xi_kpq) * I / 2.0) +
         -  pa.K * pa.S * pa.S / 8.0                                                     * gammav(neg(add(k, p))) * cexp( (xi_k + xi_p + xi_q + xi_kpq) * I / 2.0) +
         - (pa.J / 32.0 + 5 * pa.K * pa.S * pa.S / 4.0)                                  * gammav(neg(p))         * cexp( (xi_p + xi_q + xi_kpq - xi_k) * I / 2.0) +
         - (pa.D / 8.0 + 3 * pa.D_BQ * pa.S * pa.S / 4.0 + 3 * pa.K * pa.S * pa.S / 8.0)                          * cexp( (xi_q + xi_kpq - xi_k - xi_p) * I / 2.0);
  // clang-format on
}

double complex V_1112(Vector k, Vector p, Vector q, Parameters pa) {
  double xi_k = xi(k);
  double xi_p = xi(p);
  double xi_q = xi(q);
  double xi_kpq = xi(kpq(k, p, q));

  // clang-format off
  return - (pa.D / 4.0 + 3 * pa.D_BQ * pa.S * pa.S / 2.0 + 3 * pa.K * pa.S * pa.S / 4.0)                          * cexp((xi_k + xi_p - xi_q - xi_kpq) * I / 2.0)                                                                                                            +
           (pa.J / 32.0 + 5 * pa.K * pa.S * pa.S / 16.0)                                                          * cexp(-(xi_q + xi_kpq) * I / 2.0)               * (gammav(k) * cexp((xi_p - xi_k) * I / 2.0) - gammav(p) * cexp((xi_k - xi_p) * I / 2.0))                 +
            pa.K * pa.S * pa.S / 4.0                                                     * gammav(add(k, p))      * cexp(-(xi_k + xi_p + xi_q + xi_kpq) * I / 2.0)                                                                                                           +
         - (pa.J / 16.0 + 5 * pa.K * pa.S * pa.S / 8.0)                                  * gammav(neg(q))         * cexp((xi_k + xi_p + xi_q - xi_kpq) * I / 2.0)                                                                                                            +
           (pa.J / 8.0 + pa.lambda / 8.0 + 3 * pa.K * pa.S * pa.S / 4.0)                                          * cexp((xi_q - xi_kpq) * I / 2.0)                * (gammav(sub(k, q)) * cexp((xi_p - xi_k) * I / 2.0) - gammav(sub(p, q)) * cexp((xi_k - xi_p) * I / 2.0)) +
           (pa.J / 16.0 + 5 * pa.K * pa.S * pa.S / 8.0)                                  * gammav(kpq(k, p, q))   * cexp((xi_q - xi_k - xi_p - xi_kpq) * I / 2.0)                                                                                                            +
         -  pa.K * pa.S * pa.S / 4.0                                                     * gammav(neg(add(k, p))) * cexp((xi_k + xi_p + xi_q + xi_kpq) * I / 2.0)                                                                                                            +
           (pa.J / 32.0 + 5 * pa.K * pa.S * pa.S / 16.0)                                                          * cexp((xi_q + xi_kpq) * I / 2.0)                * (gammav(neg(p)) * cexp((xi_p - xi_k) * I / 2.0) - gammav(neg(k)) * cexp((xi_k - xi_p) * I / 2.0))       +
           (pa.D / 4.0 + 3 * pa.D_BQ * pa.S * pa.S / 2.0 + 3 * pa.K * pa.S * pa.S / 4.0)                          * cexp((xi_q + xi_kpq - xi_k - xi_p) * I / 2.0);
  // clang-format on
}

double complex V_1122(Vector k, Vector p, Vector q, Parameters pa) {
  double xi_k = xi(k);
  double xi_p = xi(p);
  double xi_q = xi(q);
  double xi_kpq = xi(kpq(k, p, q));

  // clang-format off
  return -((1 / 8.0) * pa.D + (3 / 4.0) * pa.D_BQ * pa.S * pa.S + (3 / 8.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p - xi_q - xi_kpq)) -
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        gammav(k) *
        cexp(I * (1 / 2.0) * (xi_p - xi_k - xi_q - xi_kpq)) -
        (1 / 8.0) * pa.K * pa.S * pa.S * gammav(add(k, p)) * cexp(-I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) -
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        gammav(neg(q)) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p + xi_q - xi_kpq)) +
        ((1 / 8.0) * pa.J + (1 / 8.0) * pa.lambda + (3 / 4.0) * pa.K * pa.S * pa.S) *
        gammav(sub(k, q)) *
        cexp(I * (1 / 2.0) * (xi_p + xi_q - xi_k - xi_kpq)) -
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        gammav(kpq(k, p, q)) *
        cexp(I * (1 / 2.0) * (xi_q - xi_k - xi_p - xi_kpq)) -
        (1 / 8.0) * pa.K * pa.S * pa.S * gammav(neg(add(k, p))) * cexp(I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) +
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        gammav(neg(p)) *
        cexp(I * (1 / 2.0) * (xi_p + xi_q + xi_kpq - xi_k)) -
        ((1 / 8.0) * pa.D + (3 / 4.0) * pa.D_BQ * pa.S * pa.S + (3 / 8.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_q + xi_kpq - xi_k - xi_p));
  // clang-format on
}

double complex V_1211(Vector k, Vector p, Vector q, Parameters pa) {
  double xi_k = xi(k);
  double xi_p = xi(p);
  double xi_q = xi(q);
  double xi_kpq = xi(kpq(k, p, q));

  // clang-format off
  return -((1 / 4.0) * pa.D + (3 / 2.0) * pa.D_BQ * pa.S * pa.S + (3 / 4.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p - xi_q - xi_kpq)) -
        ((1 / 16.0) * pa.J + (5 / 8.0) * pa.K * pa.S * pa.S) *
        gammav(k) *
        cexp(I * (1 / 2.0) * (xi_p - xi_k - xi_q - xi_kpq)) -
        (1 / 4.0) * pa.K * pa.S * pa.S * gammav(add(k, p)) * cexp(-I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) +
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p)) *
        (
            gammav(neg(q)) * cexp(I * (1 / 2.0) * (xi_q - xi_kpq)) -
            gammav(neg(kpq(k, p, q))) * cexp(I * (1 / 2.0) * (xi_kpq - xi_q))
        ) +
        ((1 / 8.0) * pa.J + (1 / 8.0) * pa.lambda + (3 / 4.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_p - xi_k)) *
        (
            gammav(sub(k, q)) * cexp(I * (1 / 2.0) * (xi_q - xi_kpq)) -
            gammav(sub(q, p)) * cexp(I * (1 / 2.0) * (xi_kpq - xi_q))
        ) +
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        cexp(-I * (1 / 2.0) * (xi_k + xi_p)) *
        (
            gammav(kpq(k, p, q)) * cexp(I * (1 / 2.0) * (xi_q - xi_kpq)) -
            gammav(q) * cexp(I * (1 / 2.0) * (xi_kpq - xi_q))
        ) +
        (1 / 4.0) * pa.K * pa.S * pa.S * gammav(neg(add(k, p))) * cexp(I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) +
        ((1 / 16.0) * pa.J + (5 / 8.0) * pa.K * pa.S * pa.S) *
        gammav(neg(p)) *
        cexp(I * (1 / 2.0) * (xi_p + xi_q + xi_kpq - xi_k)) +
        ((1 / 4.0) * pa.D + (3 / 2.0) * pa.D_BQ * pa.S * pa.S + (3 / 4.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_q + xi_kpq - xi_k - xi_p));
  // clang-format on
}

double complex V_1212(Vector k, Vector p, Vector q, Parameters pa) {
  double xi_k = xi(k);
  double xi_p = xi(p);
  double xi_q = xi(q);
  double xi_kpq = xi(kpq(k, p, q));

  // clang-format off
  return -((1 / 2.0) * pa.D + 3 * pa.D_BQ * pa.S * pa.S + (3 / 2.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p - xi_q - xi_kpq)) +
        ((1 / 16.0) * pa.J + (5 / 8.0) * pa.K * pa.S * pa.S) *
        cexp(-I * (1 / 2.0) * (xi_q + xi_kpq)) *
        (gammav(k) * cexp(I * (1 / 2.0) * (xi_p - xi_k)) - gammav(p) * cexp(I * (1 / 2.0) * (xi_k - xi_p))) +
        (1 / 2.0) * pa.K * pa.S * pa.S * gammav(add(k, p)) * cexp(-I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) +
        ((1 / 16.0) * pa.J + (5 / 8.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p)) *
        (
            gammav(neg(q)) * cexp(I * (1 / 2.0) * (xi_q - xi_kpq)) -
            gammav(neg(kpq(k, p, q))) * cexp(I * (1 / 2.0) * (xi_kpq - xi_q))
        ) +
        ((1 / 8.0) * pa.J + (1 / 8.0) * pa.lambda + (3 / 4.0) * pa.K * pa.S * pa.S) * (
            gammav(sub(q, p)) * cexp(I * (1 / 2.0) * (xi_p + xi_q - xi_k - xi_kpq)) +
            gammav(sub(p, q)) * cexp(I * (1 / 2.0) * (xi_k + xi_q - xi_p - xi_kpq)) -
            gammav(sub(k, q)) * cexp(I * (1 / 2.0) * (xi_p + xi_q - xi_k - xi_kpq)) -
            gammav(sub(q, k)) * cexp(I * (1 / 2.0) * (xi_k + xi_kpq - xi_p - xi_q))
        ) -
        ((1 / 16.0) * pa.J + (5 / 8.0) * pa.K * pa.S * pa.S) *
        cexp(-I * (1 / 2.0) * (xi_k + xi_p)) *
        (
            gammav(kpq(k, p, q)) * cexp(I * (1 / 2.0) * (xi_q - xi_kpq)) -
            gammav(q) * cexp(I * (1 / 2.0) * (xi_kpq - xi_q))
        ) + (1 / 2.0) * pa.K * pa.S * pa.S * gammav(neg(add(k, p))) * cexp(I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) -
        ((1 / 16.0) * pa.J + (5 / 8.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_q + xi_kpq)) *
        (gammav(neg(p)) * cexp(I * (1 / 2.0) * (xi_p - xi_k)) - gammav(neg(k)) * cexp(I * (1 / 2.0) * (xi_k - xi_p))) -
        ((1 / 2.0) * pa.D + 3 * pa.D_BQ * pa.S * pa.S + (3 / 2.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_q + xi_kpq - xi_k - xi_p));
  // clang-format on
}
double complex V_1222(Vector k, Vector p, Vector q, Parameters pa) {
  double xi_k = xi(k);
  double xi_p = xi(p);
  double xi_q = xi(q);
  double xi_kpq = xi(kpq(k, p, q));

  // clang-format off
  return -((1 / 4.0) * pa.D + (3 / 2.0) * pa.D_BQ * pa.S * pa.S + (3 / 4.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p - xi_q - xi_kpq)) -
        ((1 / 16.0) * pa.J + (5 / 8.0) * pa.K * pa.S * pa.S) *
        gammav(k) *
        cexp(I * (1 / 2.0) * (xi_p - xi_k - xi_q - xi_kpq)) -
        (1 / 4.0) * pa.K * pa.S * pa.S * gammav(add(k, p)) * cexp(-I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) +
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p)) *
        (
            gammav(neg(q)) * cexp(I * (1 / 2.0) * (xi_q - xi_kpq)) -
            gammav(neg(kpq(k, p, q))) * cexp(I * (1 / 2.0) * (xi_kpq - xi_q))
        ) +
        ((1 / 8.0) * pa.J + (1 / 8.0) * pa.lambda + (3 / 4.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_p - xi_k)) *
        (
            gammav(sub(q, p)) * cexp(I * (1 / 2.0) * (xi_kpq - xi_q)) -
            gammav(sub(k, q)) * cexp(I * (1 / 2.0) * (xi_q - xi_kpq))
        ) +
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        cexp(-I * (1 / 2.0) * (xi_k + xi_p)) *
        (
            gammav(kpq(k, p, q)) * cexp(I * (1 / 2.0) * (xi_q - xi_kpq)) -
            gammav(q) * cexp(I * (1 / 2.0) * (xi_kpq - xi_q))
        ) +
        (1 / 4.0) * pa.K * pa.S * pa.S * gammav(neg(add(k, p))) * cexp(I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) -
        ((1 / 16.0) * pa.J + (5 / 8.0) * pa.K * pa.S * pa.S) *
        gammav(neg(p)) *
        cexp(I * (1 / 2.0) * (xi_p + xi_q + xi_kpq - xi_k)) +
        ((1 / 4.0) * pa.D + (3 / 2.0) * pa.D_BQ * pa.S * pa.S + (3 / 4.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_q + xi_kpq - xi_k - xi_p));
  // clang-format on
}

double complex V_2211(Vector k, Vector p, Vector q, Parameters pa) {
  double xi_k = xi(k);
  double xi_p = xi(p);
  double xi_q = xi(q);
  double xi_kpq = xi(kpq(k, p, q));

  // clang-format off
  return -((1 / 8.0) * pa.D + (3 / 4.0) * pa.D_BQ * pa.S * pa.S + (3 / 8.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p - xi_q - xi_kpq)) -
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        gammav(k) *
        cexp(I * (1 / 2.0) * (xi_p - xi_k - xi_q - xi_kpq)) -
        (1 / 8.0) * pa.K * pa.S * pa.S * gammav(add(k, p)) * cexp(-I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) +
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        gammav(neg(q)) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p + xi_q - xi_kpq)) +
        ((1 / 8.0) * pa.J + (1 / 8.0) * pa.lambda + (3 / 4.0) * pa.K * pa.S * pa.S) *
        gammav(sub(k, q)) *
        cexp(I * (1 / 2.0) * (xi_p + xi_q - xi_k - xi_kpq)) +
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        gammav(kpq(k, p, q)) *
        cexp(I * (1 / 2.0) * (xi_q - xi_k - xi_p - xi_kpq)) -
        (1 / 8.0) * pa.K * pa.S * pa.S * gammav(neg(add(k, p))) * cexp(I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) -
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        gammav(neg(p)) *
        cexp(I * (1 / 2.0) * (xi_p + xi_q + xi_kpq - xi_k)) -
        ((1 / 8.0) * pa.D + (3 / 4.0) * pa.D_BQ * pa.S * pa.S + (3 / 8.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_q + xi_kpq - xi_k - xi_p));
  // clang-format on
}

double complex V_2212(Vector k, Vector p, Vector q, Parameters pa) {
  double xi_k = xi(k);
  double xi_p = xi(p);
  double xi_q = xi(q);
  double xi_kpq = xi(kpq(k, p, q));

  // clang-format off
  return -((1 / 4.0) * pa.D + (3 / 2.0) * pa.D_BQ * pa.S * pa.S + (3 / 4.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p - xi_q - xi_kpq)) +
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        cexp(-I * (1 / 2.0) * (xi_q + xi_kpq)) *
        (gammav(k) * cexp(I * (1 / 2.0) * (xi_p - xi_k)) - gammav(p) * cexp(I * (1 / 2.0) * (xi_k - xi_p))) +
        (1 / 4.0) * pa.K * pa.S * pa.S * gammav(add(k, p)) * cexp(-I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) +
        ((1 / 16.0) * pa.J + (5 / 8.0) * pa.K * pa.S * pa.S) *
        gammav(neg(q)) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p + xi_q - xi_kpq)) +
        ((1 / 8.0) * pa.J + (1 / 8.0) * pa.lambda + (3 / 4.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_q - xi_kpq)) *
        (
            gammav(sub(p, q)) * cexp(I * (1 / 2.0) * (xi_k - xi_p)) -
            gammav(sub(k, q)) * cexp(I * (1 / 2.0) * (xi_p - xi_k))
        ) -
        ((1 / 16.0) * pa.J + (5 / 8.0) * pa.K * pa.S * pa.S) *
        gammav(kpq(k, p, q)) *
        cexp(I * (1 / 2.0) * (xi_q - xi_k - xi_p - xi_kpq)) -
        (1 / 4.0) * pa.K * pa.S * pa.S * gammav(neg(add(k, p))) * cexp(I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) +
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_q + xi_kpq)) *
        (gammav(neg(p)) * cexp(I * (1 / 2.0) * (xi_p - xi_k)) - gammav(neg(k)) * cexp(I * (1 / 2.0) * (xi_k - xi_p))) +
        ((1 / 4.0) * pa.D + (3 / 2.0) * pa.D_BQ * pa.S * pa.S + (3 / 4.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_q + xi_kpq - xi_k - xi_p));
  // clang-format on
}

double complex V_2222(Vector k, Vector p, Vector q, Parameters pa) {
  double xi_k = xi(k);
  double xi_p = xi(p);
  double xi_q = xi(q);
  double xi_kpq = xi(kpq(k, p, q));

  // clang-format off
  return -((1 / 8.0) * pa.D + (3 / 4.0) * pa.D_BQ * pa.S * pa.S + (3 / 8.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p - xi_q - xi_kpq)) -
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        gammav(k) *
        cexp(I * (1 / 2.0) * (xi_p - xi_k - xi_q - xi_kpq)) -
        (1 / 8.0) * pa.K * pa.S * pa.S * gammav(add(k, p)) * cexp(-I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) +
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        gammav(neg(q)) *
        cexp(I * (1 / 2.0) * (xi_k + xi_p + xi_q - xi_kpq)) -
        ((1 / 8.0) * pa.J + (1 / 8.0) * pa.lambda + (3 / 4.0) * pa.K * pa.S * pa.S) *
        gammav(sub(k, q)) *
        cexp(I * (1 / 2.0) * (xi_p + xi_q - xi_k - xi_kpq)) +
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        gammav(kpq(k, p, q)) *
        cexp(I * (1 / 2.0) * (xi_q - xi_k - xi_p - xi_kpq)) -
        (1 / 8.0) * pa.K * pa.S * pa.S * gammav(neg(add(k, p))) * cexp(I * (1 / 2.0) * (xi_k + xi_p + xi_q + xi_kpq)) +
        ((1 / 32.0) * pa.J + (5 / 16.0) * pa.K * pa.S * pa.S) *
        gammav(neg(p)) *
        cexp(I * (1 / 2.0) * (xi_p + xi_q + xi_kpq - xi_k)) -
        ((1 / 8.0) * pa.D + (3 / 4.0) * pa.D_BQ * pa.S * pa.S + (3 / 8.0) * pa.K * pa.S * pa.S) *
        cexp(I * (1 / 2.0) * (xi_q + xi_kpq - xi_k - xi_p));
  // clang-format on
}
