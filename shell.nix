let
  pkgs = import <nixpkgs> { };
  rubyEnv = pkgs.bundlerEnv {
    name = "msc-project-dissertation";
    gemdir = ./.;
  };
  texEnv = with pkgs.texlive;
    combine {
      # TODO: Check which of these are actually used.
      inherit scheme-basic biber biblatex collection-xetex geometry mathtools
        parskip;
      # TODO: What is this a dependency of?
      inherit etoolbox;
    };
in pkgs.mkShell {
  buildInputs = with pkgs; [
    inkscape
    julia_16-bin
    rubber
    rubyEnv
    rubyEnv.wrappedRuby
    texEnv
  ];

  LD_LIBRARY_PATH = "${pkgs.addOpenGLRunpath.driverLink}/lib";
}
